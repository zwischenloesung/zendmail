# zendmail

Acts a little like the classic sendmail, but supports multiple smtp-user accounts. An (optionally) encrypted account store holds the credentials.